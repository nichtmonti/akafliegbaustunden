<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddedAnwesenheitDataToPalaver extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('palaver', function (Blueprint $table) {
            $table->binary('anwesenheit');
        }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('palaver', function ($table) {
            $table->dropColumn('palaver');
        });
    }
}
