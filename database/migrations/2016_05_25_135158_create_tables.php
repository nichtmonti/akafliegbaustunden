<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Tables

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('nickname')->nullable();
            $table->Integer('status_id')->unsigned();
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamp('created_at');
        });

        Schema::create('entries', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('description');
            $table->integer('user_id')->unsigned();
            $table->integer('palaverItem_id')->unsigned();
            $table->decimal('work_time');

            $table->timestamps();
        });

        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->boolean('isAirplane');
            $table->boolean('flightReady')->nullable();
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::create('statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->decimal('required_hrs');
            $table->timestamps();
        });

        Schema::create('entry_helpers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('entry_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

        });


        Schema::create('authentification', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('password');
            $table->boolean('admin');
            $table->rememberToken();
            $table->timestamps();


        });

        Schema::create('palaverItems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->string('status')->default('');
            $table->boolean('done');
            $table->boolean('canceled');
            $table->integer('project_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('projects');
            $table->timestamps();

        });

        Schema::create('palaverItem_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('palaverItem_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });

        //keys

        Schema::table('entries', function ($table) {

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('palaverItem_id')->references('id')->on('palaverItems');

        });

        Schema::table('entry_helpers', function ($table) {

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('entry_id')->references('id')->on('entries')->onDelete('cascade');

        });

        Schema::table('users', function ($table) {

            $table->foreign('status_id')->references('id')->on('statuses');

        });

        Schema::table('entries', function ($table) {


        });

        Schema::table('palaverItem_user', function ($table) {

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('palaverItem_id')->references('id')->on('palaverItems')->onDelete('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        Schema::drop('password_resets');
        Schema::drop('entries');
        Schema::drop('projects');
        Schema::drop('statuses');
        Schema::drop('project_helpers');
        Schema::drop('authentification');
        Schema::drop('palaverItems');
        Schema::drop('palaverItem_user');





    }
}
