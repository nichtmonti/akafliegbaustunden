<?php

use Illuminate\Database\Migrations\Migration;

class AddNextEntschuldigtToUser extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('users', function ($table) {
            //speichert, ob user sich für nächstes Palaver entschuldigen lässt
            $table->boolean('next_palaver_entschuldigt')->after('status_id')->default(0);

        }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down() {
        Schema::table('users', function ($table) {
            $table->dropColoumn('next_palaver_entschuldigt');

        }
        );
    }
}
