<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
use App\models\User;
use Faker\Generator as Faker;

$factory->define(App\models\User::class, function (Faker $faker) {
    return [
            'first_name'                => $faker->firstName,
            'last_name'                 => $faker->lastName,
            'next_palaver_entschuldigt' => $faker->boolean(20),
            'active'                    => $faker->boolean(65),
            'email'                     => $faker->email(),
            'admin'                     => 0,
            'flugverbot'                => $faker->boolean(10),
            'palaver'                   => $faker->boolean(15),
            'status_id'                 => \App\models\status::inRandomOrder()->first()->id,
            'nickname'                  => $faker->userName,
            'password'                  => bcrypt($faker->text(12)),
            'remember_token'            => Null
    ];
});


$factory->define(\App\models\entry::class, function(Faker $faker){
   return [
            'date'                      => \Carbon\Carbon::today(),
            'description'               => $faker->text(),
            'user_id'                   => User::inRandomOrder()->first()->id,
            'palaverItem_id'            => \App\models\palaverItem::inRandomOrder()->first()->id,
            'work_time'                 => $faker->numberBetween(1,400),
            'created_at'                => \Carbon\Carbon::today(),
   ];
});

$factory->defineAs(App\models\User::class, 'admin', function (Faker $faker) {
    $user = factory(User::class)->raw();
    return array_merge($user, ['admin' => true]);

});

$factory->defineAs(\App\Http\Requests\entry_request::class, 'valid', function (Faker $faker){
    return [
        'date'                      => \Carbon\Carbon::today(),
        'description'               => $faker->text(),
        'user_id'                   => User::first()->id,
        'palaverItem_id'            => \App\models\palaverItem::first()->id,
        'hours'                     => $faker->numberBetween(1,10),
        'minutes'                   => $faker->numberBetween(0,59)
    ];
});

$factory->define(\App\models\status::class, function (Faker $faker){
   return [
       'name'           => $faker->text(15),
       'required_hrs'   => $faker->randomFloat(2,12,45)
   ];
});

$factory->define(\App\models\project::class, function(Faker $faker){
    $airplane = $faker->boolean(80);
    if($airplane){
        $flight_ready = $faker->boolean(60);
    }
    else{
        $flight_ready = False;
    }

    return [
        'name'          => $faker->text(15),
        'description'   => $faker->realText(),
      //  'user_iphpd'       => User::inRandomOrder()->first()->id,
        'isAirplane'    => $airplane,
        'flightReady'   => $flight_ready,
        'active'        => $faker->boolean(85),
        'EDEP'          => 0
    ];
});


$factory->define(\App\models\palaverItem::class, function (Faker $faker){
   return [
       'title'          => $faker->word,
       'description'    => $faker->text(255),
       'status'         => $faker->text(200),
       'done'           => $faker->boolean(40),
       'canceled'       => $faker->boolean(5),
       'project_id'     => \App\models\project::inRandomOrder()->first()->id,
       'date'           => \Carbon\Carbon::today()->toDateString()
   ];
});
