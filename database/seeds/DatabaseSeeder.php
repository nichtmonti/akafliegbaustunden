artisan<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
        statusSeeder::class,
        projectSeeder::class,
            userSeeder::class
    ]);

    }
}
