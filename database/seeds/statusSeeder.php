<?php

use Illuminate\Database\Seeder;

class statusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\models\status::class, 8)->create();

        // ausgetreten status für Test
        $status = factory(\App\models\status::class)->make();
        $status->name = 'Ausgetreten';
        $status->save();

        // Alter Sack status für Test
        $status = factory(\App\models\status::class)->make();
        $status->name = 'Alter Sack';
        $status->save();
    }
}