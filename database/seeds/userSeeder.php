<?php

use Illuminate\Database\Seeder;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $palaverItems = \App\models\palaverItem::all()->filter(function ($item){
           return !$item->user()->exists();
        });

        $projects= \App\models\project::all()->filter(function ($item){
            return !$item->supervisor()->exists();
        });



        factory(\App\models\User::class,35)->create()->each(function ($user) use ($palaverItems, $projects){
            // entries dazu
            for($x = 0; $x < 35; $x++){
            /* @var \App\models\User $user */
            $user->entries()->save(factory(\App\models\entry::class)->make());
            }

            //attach some PalaverItems
           $faker = \Faker\Factory::create();
            for ($x = 0; $x<$faker->numberBetween(1,3); $x++) {
                $user->palaverItem()->save($palaverItems->random());
            }

            //attach some Projects
            /* @var \Illuminate\Database\Eloquent\Collection $projects */
            if (!$projects->isEmpty()){
                $projects->first()->supervisor()->associate($user);
            }

        });

        $admin_user = factory(\App\models\User::class)->create();
        $admin_user->last_name = 'admin';
        $admin_user->admin=1;
        $admin_user->password=bcrypt('1234');
        $admin_user->save();
    }
}
