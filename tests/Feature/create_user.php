<?php

namespace Tests\Feature;

use App\models\User;
use Faker\Generator;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker as Faker;

class create_user extends DuskTestCase
{
    /**
     * This test verifies a new user can be created through the web interface
     *
     * @return void
     */
    public function testCreateUser()
    {
            $f = Faker\Factory::create();
            $first_name = $f->firstName;
            $last_name = $f->lastName;
            $mail = $f->email;

            $this->browse(function (Browser $browser) use ($mail,$first_name,$last_name){
                $user_login = factory(User::class, 'admin')->create();
                $browser->loginAs($user_login);
                $browser->visit('/register');
                $browser->pause(3000);
                $browser->assertSee('Mitglied Hinzufügen');
                $browser->type('first_name', $first_name);
                $browser->type('last_name', $last_name);
                $browser->type('email', $mail);
                $browser->type('password','123uipoilasd123123');
                $browser->type('password_confirmation','123uipoilasd123123');


                $browser->press('Speichern');
                $browser->assertPathBeginsWith('/user');

            });

            $users = User::where('first_name', $first_name)->where('last_name',$last_name)->get();
            self::assertEquals(1, $users->count());
            /** @var User $user */
            $user = $users->first();

            self::assertEquals(0, $user->is_admin);
            self::assertEquals($first_name, $user->first_name);
            self::assertEquals($last_name, $user->last_name);
            self::assertEquals($mail, $user->email);
            self::assertEquals(0, $user->flugberechtigt);

            $user->delete();

    }

}
