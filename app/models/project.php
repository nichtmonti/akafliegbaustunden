<?php

namespace App\models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
/**
 * App\models\project
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property boolean $isAirplane
 * @property boolean $flightReady
 * @property boolean $active
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\entry[] $entries
 * @property-read \App\models\User $supervisor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\palaverItem[] $palaverItems
 * @property-read mixed $total_time
 * @property-read mixed $flight_ready_string
 * @method static \Illuminate\Database\Query\Builder|\App\models\project whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\project whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\project whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\project whereIsAirplane($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\project whereFlightReady($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\project whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\project whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\project whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\project whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\project active()
 * @method static \Illuminate\Database\Query\Builder|\App\models\project airplane()
 * @method static \Illuminate\Database\Query\Builder|\App\models\project flightReady()
 * @mixin \Eloquent
 * @property-read mixed $supervisor_string
 * @property-read mixed $work_this_week
 */
class project extends Model {
    protected $table = 'projects';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('orderBy', function ($builder) {
            $builder->orderBy('name', 'asc');
        });


        static::addGlobalScope('EDEP', function ($builder) {
            $builder->where('EDEP', 0);
        });

    }

    public function entries() {
        return $this->hasManyThrough('App\models\entry', 'App\models\palaverItem', 'project_id', 'palaverItem_id');
    }

    public function supervisor() {
        return $this->belongsTo('App\models\user', 'user_id', 'id');
    }

    public function palaverItems() {
        return $this->hasMany('App\models\palaverItem');
    }

    public function scopeActive($query) {
        return $query->where('active', '1');
    }


    public function getTotalTimeAttribute() {
        return $this->entries->sum('work_time');
    }


    public function getTotalTime($year) {
        $dates = \Help::getStartEndDates($year);
        $start_date = $dates[0];
        $end_date = $dates[1];

        $entries = $this->entries;

        $entries = $entries->filter(function ($item) use ($start_date, $end_date) {
            $prev = Carbon::parse($item->date)->gte($start_date);
            $follow = Carbon::parse($item->date)->lte($end_date);
            return $prev && $follow; //keep item if changed in that time frame
        });

        return $entries->sum('work_time');
    }

    public function scopeAirplane($query) {
        return $query->where('isAirplane', '1');
    }

    public function scopeFlightReady($query) {
        return $query->where('flightReady', '1');
    }

    public function getFlightReadyStringAttribute() {
        if(empty($this->flightReady)){
            return "nein";
        }

        if ($this->flightReady) {
            return "ja";
        }
       
        return "nein";
    }

    public function getSupervisorStringAttribute() {
        $user = $this->supervisor;
        if (empty($user)) {
            return "Keiner";
        }
        return $user->short_name;
    }

    public function open_issues() {

        return $this->palaverItems->filter(function (palaverItem $item) {
            return $item->active();
        });
    }

    /*
     * @return Minutes worked on the project this week
     */

    public function getWorkThisWeekAttribute() {

        return $this->palaverItems->sum('work_this_week');
    }

    public function getWorkThisMonthAttribute() {

        return $this->palaverItems->sum('work_this_month');
    }

    public function getWorkThisYearAttribute() {
        return $this->palaverItems->sum('work_this_year');
    }

}
