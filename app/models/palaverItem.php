<?php

namespace App\models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;


/**
 * App\models\palaverItem
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $status
 * @property boolean $done
 * @property boolean $canceled
 * @property integer $project_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $date
 * @property-read \App\models\project $project
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\User[] $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\models\entry[] $entries
 * @property-read mixed $real_status
 * @property-read mixed $responsible_users
 * @property-read mixed $formatted_work_time
 * @property-read mixed $total_work_time
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaverItem whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaverItem whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaverItem whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaverItem whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaverItem whereDone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaverItem whereCanceled($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaverItem whereProjectId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaverItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaverItem active()
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaverItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaverItem whereDate($value)
 * @mixin \Eloquent
 * @property-read mixed $work_this_week
 * @method static \Illuminate\Database\Query\Builder|\App\models\palaverItem activeScope()
 * @property-read mixed $active
 */
class palaverItem extends Model {
    protected $fillable = [
            'title', 'description', 'nickname', 'status', 'project_id', 'done', 'canceled', 'date'
    ];

    protected $table = 'palaverItems';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('orderBy', function ($builder) {
            $builder->orderBy('title', 'asc');
        });
    }

    public function project() {
        return $this->belongsTo('App\models\project');
    }

    public function user() {
        return $this->belongsToMany('App\models\user', 'palaverItem_user', 'palaverItem_id', 'user_id');
    }

    public function entries() {
        return $this->hasMany('App\models\entry', 'palaverItem_id');
    }

    public function getRealStatusAttribute() {
        //return $this->status;
        if ($this->done) {
            return "Fertig";
        }

        if ($this->canceled) {
            return "Abgebrochen";
        }

        if (!$this->status == '') {
            return $this->status;
        }

        if ($this->entries->isEmpty()) {
            return "Noch nichts passiert";
        }


        return $this->entries->sortBy('date')->first()->description;
    }



    //gibt Nutzer als String zurück (Nutzer1, Nuter2, ...
    public function getResponsibleUsersAttribute() {

        $users = $this->user;
        if ($users->isEmpty()) {
            return "Niemand";
        }
        $result = "";
        for ($x = 0; $x < sizeof($users) - 1; $x++) {
            $result = $result . $users[$x]->short_name . ', ';
        }
        return $result . $users[sizeof($users) - 1]->short_name;

    }

    public function getActiveAttribute() {
        return !($this->canceled || $this->done);
    }

    //gibt nur nicht abgeschlossene Projekte oder solche, die in der vergangenen 6 Tagen abgescshlossen wurden
    public function scopeActive($query) {
        return $query->where(function ($query) {
            $query->where('done', 0)->where('canceled', 0);
        })->orwhere('updated_at', '>', Carbon::now()->subWeek()->addDay()->toDateString());
    }

    public function getFormattedWorkTimeAttribute() {
        $time = $this->getTotalWorkTimeAttribute();
        $hours = floor($time / 60);
        $minutes = $time % 60;
        return $hours . " Stunden, " . $minutes . " Minuten";
    }

    public function getTotalWorkTimeAttribute() {
        return $this->entries->sum('work_time');
    }

    public function getTotalWorkTime($year) {

        $entries = $this->entries;

        return $entries->sum('work_time');
    }

    public function getWorkThisWeekAttribute() {
        $cache_key = $this->id.'palaverItemWorkThisWeek';

            if(Cache::has($cache_key)){
                return Cache::get($cache_key);

            }

            $workThisWeek = $this->entries->filter(function (entry $entry) {
                return Carbon::parse($entry->date)->gte(Carbon::today()->startOfWeek());
            })->sum('work_time');

            $expires_at = Carbon::now()->addWeek();
            Cache::put($cache_key, $workThisWeek, $expires_at);

            return $workThisWeek;



    }

    public function getWorkThisMonthAttribute() {
        $cache_key = $this->id.'palaverItemWorkThisMonth';

        if(Cache::has($cache_key)){
            return Cache::get($cache_key);
        }

        $workThisMonth = $this->entries->filter(function(entry $entry){
            return Carbon::parse($entry->date)->gte(Carbon::today()->startOfMonth());
        })->sum('work_time');

        $expires_at = Carbon::now()->addWeek();
        Cache::put($cache_key, $workThisMonth, $expires_at);
        return $workThisMonth;

    }

    public function getWorkThisYearAttribute() {
        $cache_key= $this->id.'palaverItemWorkThisYear';

        if(Cache::has($cache_key)){
            return Cache::get($cache_key);
        }

        $workThisYear = $this->entries->filter(function (entry $entry) {
                return Carbon::parse($entry->date)->gte(Carbon::create(\Help::currentYear() - 1, env('BEGIN_YEAR_MONTH'), 01, 0, 0));
            })->sum('work_time');

        $expires_at = Carbon::now()->addWeek();
        Cache::put($cache_key, $workThisYear, $expires_at);


        return $workThisYear;

    }



}
