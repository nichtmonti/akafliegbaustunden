<?php

namespace App\Policies;

use App\models\car;
use App\models\user;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class carPolicy {
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function addPassenger(User $user, car $car) {
        if($user->drivesCar($car->date)){
       return $this->deny("Du fährst bereits mit einem anderen Auto");
        }
        //Falls Auto voll ist
        elseif($car->is_full){
            return $this->deny("Das Auto ist voll");
        }
       return $this->allow();
    }

    public function addMultiplePassengers(User $user, car $car) {
        return $user->id === $car->fahrer->id;
    }

    //used from blade template
    public function create(User $user,$date ) {
        $dummyCar= new  car();
        $dummyCar->date=$date;

        //check for valid date
        return $this->store($user,$dummyCar);
    }


    public function store(User $user,car $car) {
        //if  user does not already drive a different car && date is legit
        if($user->drivesCar($car->date)){
            return $this->deny('Du fährst heute schon mit einem anderen Auto');
        }
        //Wenn Datum in Vergangenheit

        elseif(!Carbon::parse($car->date)->gt(Carbon::today())){
            return $this->deny('Das Datum liegt in der Vergangenheit');
        }
        //Wenn Auto schon voll
        elseif ($car->seats < $car->mitfahrer->count()) {
            return $this->deny('Das Auto ist überfüllt');
        }

        return $this->allow();

    }
    public function update(User $user, car $car) {
        return $car->fahrer->id === $user->id;

    }

    public function destroy(User $user,car $car) {
        if($user->id == $car->user_id){
            return $this->allow();
        }
        return $this->deny("Du kannst nur dein eigenes Auto löschen");
    }

    public function remove_mitfahrer(User $user, car $car, User $mitfahrer) {
        return $mitfahrer->id === $user->id || $car->fahrer->id === $user->id;

    }
}