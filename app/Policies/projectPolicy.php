<?php

namespace App\Policies;

use App\models\project;
use App\models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class projectPolicy {
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct() {

    }

    public function create(User $user) {
        return $this->store($user);
    }

    public function store(User $user) {
        if($user->is_admin){
            return $this->allow();
        }
        if($user->can_palaver){
            return $this->allow();
        }
        return $this->deny('Du musst Palaver leiten dürfen um diese Aktion durchzuführen');

    }

    public function update(User $user, project $project) {
        if($user->is_admin){
            return $this->allow();
        }

        //nur admin darf projekt umbenennen
        if ($project->name != $_REQUEST['name']) {
            return $this->deny('Nur Admin darf Projekt umbenennen');
        }
        if ($project->user_id != $user->id && !$user->can_palaver) {
            return $this->deny('Du darfst den Betreuer nicht ändern');
        }

        return $this->allow();
    }


}
