<?php

namespace App\Console\Commands;

use App\models\status;
use Illuminate\Console\Command;
use App\models\User;

class create_root_user extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create_root_user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a root user for further setup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $last_name = $this->ask('Nachname?');
        $first_name= $this->ask('Vorname?');
        $password = $this->secret('Passwort?');

        $default_status = new status();
        $default_status->name = 'default';
        $default_status->required_hrs =0;
        $default_status->save();

        $ausgetreten = new status();
        $ausgetreten->name = 'Ausgetreten';
        $ausgetreten->required_hrs=0;
        $ausgetreten->save();

        $u = new User();
        $u->last_name = $last_name;
        $u->first_name = $first_name;
        $u->password = bcrypt($password);
        $u->status_id = $default_status->id;
        $u->admin = 1;
        $u->save();
    }
}
