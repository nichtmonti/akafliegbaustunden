<?php

namespace App\Http\Controllers;

use App\models\status;
use App\models\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class user_controller extends Controller
{
    public function __construct() {
    }

    public function index(Request $request)
    {

        $this->authorize('index', Auth::user());


        //zeige nur Nutzer der jungen Gruppe
        if (!isset($request->show_all_users) || $request->show_all_users==0) {
            $users = user::with('entries', 'versions', 'status')->jungeGruppe()
                ->paginate(20);
        }

        //auch ehemalige Nutzer
        //TODO: funktionalität ins frontend einbinden
        else{
            $users = user::with('entries', 'versions', 'status')
                ->paginate(20);
        }


        if (isset($request->year)) {
            $year = $request->year;
        } else {
            $year = \Help::currentYear();
        }

        return view('user.index')->with('users', $users)->with('year', $year)->with($request->all()); //eager load versions and statuses
    }


    public function create() {
        //display form to create the user
        //TODO User::class funktioniert nicht
        $this->authorize('create', new User());

        return view('user.create')->with('statuses', status::pluck('name', 'id'));
    }

    public function update(Request $request,User $user) {

        \Log::info(\Auth::user()->last_name.", ".\Auth::user()->first_name." is editing ".$user->first_name. " ".$user->last_name." with the request".
        $request->getContent());
        //if user is not edititng himself && is not admin return
        $this->authorize('edit', $user);

        //updates the user model
        if (\Auth::user()->is_admin) {
            if (!isset($request->active)) {
                $user->active = 0;
            } elseif (isset($request->active)) {
                $user->active = 1;
            }
        }

        //if admin is submitting form, edit admin and canPalaver fields
        if (Auth::check() && Auth::user()->is_admin) {
            if (!isset($request->admin)) {
                $user->admin = 0;
            } else {
                $user->admin = $request->admin;
            }
            if (!isset($request->palaver)) {
                $user->palaver = 0;
            } else {
                $user->palaver = $request->palaver;
            }

            $user->email = $request->email;

            isset($request->flugverbot) ? $user->flugverbot = 1 : $user->flugverbot = 0;

            $user->flugverbot_kommentar = $request->flugverbot_kommentar;

        }
        //if password is being changed
        if ($request->password != "") {
            $this->validate($request, [
                    'password' => 'required|min:6|confirmed',
            ]);

            $user->password = bcrypt($request->password);
        }

        if (!$request->next_palaver_entschuldigt) {
            $user->next_palaver_entschuldigt = 0;
        } else {
            $user->next_palaver_entschuldigt = 1;
        }
        if (isset($request->date
        )) {
            $user->created_at = $request->date;
        }


        //berehtigungen können nur durch admin geändert werden
        if ($user->update($request->except('password', 'active', 'admin', 'palaver', 'next_palaver_entschuldigt', 'date'))) {
            Session::flash('alert-success', 'Eintrag aktualisiert');
        } else {
            Session::flash('alert-danger', 'Das hat nicht funktioniert');
        }

        return redirect('/user/'.$user->id);
    }


    /**
     * @param user $user       der angeizeigt werden soll
     * @param Request $request enthält das Jahr
     *
     * @return mixed
     */
    public function show(User $user, Request $request) {

        $this->authorize('show',$user,Auth::user());

        if (env('ALLOW_PAST_YEARS')) { //wenn nur das aktuelle Geschäftsjahr angezeigt werden können soll
            if (isset($request->year)) {
                $year = $request->year;
            } else { //if year is not set
                $year=\Help::currentYear();
            }
        } else {
            $year = \Help::currentYear();
        }

        $dates = \Help::getStartEndDates($year);
        $start_date=$dates[0]->toDateString();
        $end_date=$dates[1]->toDateString();
        $user->load('versions', 'status');

        return view('user.view')->with('user', $user)->with('entries', $user->entries()->year($year)
                ->with('palaverItem.project', 'helpers', 'user')->paginate(15))->with('year', $year)->with('edit',$request->edit);
    }

    public function store(Request $request) {
        //add the user
        // dd(isset($request->active));

        $this->validate($request, [
                'first_name' => 'required',
                'nickname'   => 'unique:users',
                'last_name'  => 'required',
                'status_id'  => 'required'
        ]);

        $new_user = new User();
        $new_user->first_name = $request->first_name;
        $new_user->nickname = $request->nickname;
        $new_user->last_name = $request->last_name;
        $new_user->status_id = $request->status_id;
        $new_user->email = $request->email;
        if (isset($request->active)) {
            $new_user->active = 1;
        }

        if (Auth::user()->is_admin) {

            isset($request->admin) ? $new_user->admin = 1 : $new_user->admin = 0;

            isset($request->palaver) ? $new_user->palaver = 1 : $new_user->palaver = 0;

            isset($request->flugverbot) ? $new_user->flugverbot = 1 : $new_user->flugverbot = 0;

            $new_user->flugverbot_kommentar = $request->flugverbot_kommentar;
        }

        $this->validate($request, [
                'password' => 'required|min:6|confirmed',
        ]);

        $new_user->password = bcrypt($request->password);
        $this->authorize('create',$new_user);
        if (isset($request->date)) {
            $new_user->created_at = $request->date;
        }

        $new_user->save();


        //TODO test if this works
        return redirect('/user/' . $new_user->id);
    }

    public function destroy(User $user) {
        return;
    }

    public function edit(User $user) { //displays form to edit user
        //if user is not edititng himself && is not admin return
        //if user is not edititng himself && is not admin return

        $this->authorize('edit', $user);


        return view('user.edit')->with('user', $user)->with('statuses', status::pluck('name', 'id'));
    }

    public function show_email_overview() {
        return view('user.email_overview')->with('users', User::all());
    }

}
