<?php

namespace App\Http\Controllers;

use App\models\palaver;
use App\models\palaverItem;
use App\models\project;
use App\models\User;
use Barryvdh\Snappy;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
use Session;

class palaver_controller extends Controller {

    public function __construct() {
        $this->middleware('canPalaver_auth', ['only' => ['create_anwesenheit', 'save_data', 'editAnwesenheit', 'updateAnwesenheit', 'save_anwesenheit', 'save_data']]);
    }

    public function create(Request $request) {

        if (isset($request->project)) {
            return view('palaverItem.create')->with('projects', project::pluck('name', 'id'))->with('users', User::get()->pluck('full_name', 'id'))->with('selected', $request->project);
        }

        return view('palaverItem.create')->with('projects', project::pluck('name', 'id'))->with('users', User::get()->pluck('full_name', 'id'));
    }

    public function store(Request $request) {

        $this->validate($request, [
                'title'       => 'required',
        ]);

        $palaverItem = new palaverItem($request->all());

        //Date als null speichern, wenn Feld leer
        if ($request->date == "" || $request->no_deadline) {
            $palaverItem->date = NULL;
        }

        isset($request->done) ? $palaverItem->done = 1 : $palaverItem->done = 0;


        isset($request->canceled) ? $palaverItem->canceled = 1 : $palaverItem->canceled = 0;

        $this->authorize('store',$palaverItem);

        $success = $palaverItem->save();

        $palaverItem->user()->attach($request->user_id); //saves helpers

        if ($success) {
            Session::flash('alert-success', 'Eintrag hinzugefügt');
        } else {
            Session::flash('alert-danger', 'Das hat nicht funktioniert');
        }

        Session::flash('hook', $palaverItem->id);

        if (isset($request->type)) {
            if ($request->type == 'p') {
                return redirect('/palaver_view');
            } else if ($request->type == 'np') {
                return redirect('/palaverItem');
            }
        }
        return redirect('palaverItem/' . $palaverItem->id);
    }

    public function edit(palaverItem $palaverItem) {

        $this->authorize('edit', $palaverItem);

        return view('palaverItem.edit')->with('palaverItem', $palaverItem)->with('projects', project::pluck('name', 'id'))->with('users', User::get()->pluck('full_name', 'id'));
    }

    public function update(Request $request, palaverItem $palaverItem) {

        $this->validate($request, [
                'title'       => 'required',
        ]);

        $this->authorize('update', $palaverItem);

        if ($request->canceled && $request->done) {
            Session::flash('alert-warning', 'Es kann nicht gleichzeitig fertig und abgebrochen sein');
            return back()->withInput();
        }

        if (!isset($request->done)) {
            $palaverItem->done = 0;
        }
        if (!isset($request->canceled)) {
            $palaverItem->canceled = 0;
        }

        if (\Auth::user()->is_admin) {
            $palaverItem->user()->detach();
            $palaverItem->user()->attach($request->user_id);
        } else if (isset($request->user_id)) {
            $palaverItem->user()->detach();
            $palaverItem->user()->attach($request->user_id); //saves helpers
        } else {
            Session::flash('alert-warning', 'Es kann nicht Keiner verantwortlich sein');
            return back()->withInput();
        }

        //Date als null speichern, wenn Feld leer
        if ($request->date == "" || $request->no_deadline) {
            $palaverItem->date = NULL;
            $palaverItem->save();
        }

        if ($palaverItem->update($request->all())) {
            Session::flash('alert-success', 'Eintrag aktualisiert');
        } else {
            Session::flash('alert-danger', 'Das hat nicht funktioniert');
        }

        Session::flash('hook', $palaverItem->id);

        if (isset($request->type)) {
            if ($request->type == 'p') {
                return redirect('/palaver_view/');
            } elseif ($request->type == 'np') {
                return redirect('/palaverItem/'.$palaverItem->id);
            }
        }

        return redirect('/palaverItem/'.$palaverItem->id);//TODO Das geht auch besser

    }

    /**
     * called on route /palaver/create. Checks for unfinished Palaver and routes accordingly.
     */
    public function create_palaver() {

        if (palaver::isPalaver()) {
            return redirect('/palaver_view');
        } else {
            return $this->create_anwesenheit();
        }

    }

    public function create_anwesenheit() {
        if (Gate::denies('palaver')) {
            abort(403, "Unauthorized");
        }

        return view('palaver.palaver_anwesenheit')->with('users', User::all()); //TODO sinnvolles scope für User einrichten
    }

    public function editAnwesenheit(palaver $palaver) {
        return abort('veraltet');
        if (Gate::denies('palaver')) {
            abort(403, "Unauthorized");
        }
        return view('palaver.palaver_anwesenheit_edit')->with('users', User::all())->with('palaver', $palaver);
    }

    public function updateAnwesenheit(Request $request, palaver $palaver) {
        return abort('Veraltet');
        $anwesend = [];
        $entschuldigt = [];
        foreach ($request->all() as $key => $value) {
            if ($value == 2) {
                array_push($anwesend, $key);
            } else if ($value == 1) {
                array_push($entschuldigt, $key);
            }
        }
        $palaver->anwesende()->detach();
        $palaver->entschuldigte()->detach();
        $palaver->anwesende()->attach($anwesend);
        $palaver->entschuldigte()->attach($entschuldigt);

        return redirect('/palaverItem/palaver');
    }

    public function save_anwesenheit(Request $request) {
        if (Gate::denies('palaver')) {
            abort(403, "Unauthorized");
        }
        $palaver = new palaver();
        $palaver->date = Carbon::today()->toDateString();
        $anwesend = [];
        $entschuldigt = [];
        foreach ($request->all() as $key => $value) {
            if ($value == 2) {
                array_push($anwesend, $key);
            } else if ($value == 1) {
                array_push($entschuldigt, $key);
            }
        }
        $anwesenheit = array($anwesend, $entschuldigt);
        $palaver->anwesenheit = serialize($anwesenheit);

        $palaver->save();

        //Entschuldigungen zurücksetzen
        DB::table('users')->update(['next_palaver_entschuldigt' => 0]);


        return redirect('/palaver_view');
    }

    public function palaver_show(palaver $palaver) {
        return view('palaver.view')->with('users', User::all())->with('palaver', $palaver);
    }

    public function show(palaverItem $palaverItem) {

        $palaverItem= $palaverItem->load('entries');

        $time =$palaverItem->entries->sum('work_time');

        $entries=$palaverItem->entries()->with('user')->paginate(25);

        return view('palaverItem.view')->with('entries', $entries)->with('time', $time)->with('palaverItem',$palaverItem);
    }

    public function palaverIndex() {
        return view('palaver.index')->with('palavers', palaver::all()->get()); //TODO gutes scope
    }

    /**
     * Saves Current status of projects/user/ status to database
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save_data(Request $request) {
        //TODO user sichern, projekte sichern, projekt betreuer anzeigen

        $palaver = palaver::aktuell();

        $user = User::all();

        $projects = project::with(['palaverItems' => function ($q) {
            return $q->active();
        }, 'palaverItems.user'])->get();
        $projects->load('supervisor');
        $data = array($projects, $user, $request->Anmerkungen);
        $palaver->data = serialize($data);
        $palaver->save();

        //redirect and display
        return redirect('/palaver/' . $palaver->id);
    }

    public function pdf(palaver $palaver) {
        //Decode data
        $data = unserialize($palaver->data);
        $anwesenheit = unserialize($palaver->anwesenheit);
        $projects = collect($data[0]);
        $user = collect($data[1]);
        $anmerkung = collect($data[2])->first();
        $anwesende = collect($anwesenheit[0]);
        $entschuldigte = collect($anwesenheit[1]);

        //return the view
        $html = \View::make('palaver.pdf')->with('users', $user)->with('date', Carbon::parse($palaver->date)->format("d. M Y"))->with('projects', $projects)->with('anwesende', $anwesende)->with('entschuldigte', $entschuldigte)->with('anmerkung', $anmerkung);
        return $html;

    }

    public function index() {
        //OLD INDEX
        /*  $palaverStuff = project::with(['palaverItems' => function ($q) {
              $q->active();
          }])->with('palaverItems.entries')->with('palaverItems.entries.user')->with('palaverItems.user')->get();


          return view('palaverItem.index_nopalaver')->with('projects', $palaverStuff);*/

        //new index
        return redirect('/project');

    }

    public function palaver() {
        if (!palaver::isPalaver()) {
            return response('Noch keine Anwesenheit festgestellt', 403);

        }

        $palaverStuff = project::with(['palaverItems' => function ($q) {
            $q->active();
        }])->with('palaverItems.entries')->with('palaverItems.entries.user')->with('palaverItems.user')->get();


        return view('palaverItem.index_palaver')->with('projects', $palaverStuff);
    }
}
