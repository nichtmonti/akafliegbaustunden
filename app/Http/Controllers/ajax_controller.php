<?php

namespace App\Http\Controllers;

use App\models\car;
use App\models\palaverItem;
use App\models\user;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;


class ajax_controller extends Controller {
    //TODO check if Auth::User() can be used here to validate permission to set next_palaver_entschuldigt

    public function palaver(PalaverItem $palaverItem, Request $request) {
        Log::info($request->date);
        if (Input::get('action') == 'edit_all') {
            //save description
            $palaverItem->description = Input::get('description');

            //save helpers
            $palaverItem->user()->detach();
            $palaverItem->user()->attach(Input::get('helpers'));

            //save status
            $new_status = Input::get('status');
            if ($new_status != "Noch nichts passiert" && $new_status != "Fertig" && $new_status != "Abgebrochen") {
                $palaverItem->status = $new_status;
            }

            //save date
            if($request->date==""){
                $palaverItem->date=NULL;
            }
            else {
                $palaverItem->date = $request->date;
            }

            $palaverItem->save();
            $result = [];
            $newPalaverItem = palaverItem::where('id', $palaverItem->id)->get()->first();


            array_push($result, $palaverItem->responsible_users, $newPalaverItem->description, $newPalaverItem->real_status, $newPalaverItem->date);
            return $result;

        }
        //Toggles done status
        if (Input::get('action') == 'status') {
            if ($palaverItem->done) {
                $palaverItem->done = 0;
            } else {
                $palaverItem->done = 1;
            }
            $palaverItem->save();
            return palaverItem::where('id', $palaverItem->id)->get()->first()->real_status;
        }


        if (Input::get('action') == 'description') {
            $palaverItem->description = Input::get('description');
            $palaverItem->save();
            return \App\models\palaverItem::where('id', $palaverItem->id)->get()->first()->description;

        }

        if (Input::get('action') == 'user_form') {
            $html = View::make('partials.multiselect')->with('palaverItem', $palaverItem)->with('users', User::get()->pluck('full_name', 'id'))->render();
            return $html;
        }

        if (Input::get('action') == 'save_helpers') {

            $palaverItem->user()->detach();
            $palaverItem->user()->attach(Input::get('helpers')); //saves helpers
            Log::info(Input::get('helpers'));

            return $palaverItem->responsible_users;
        }

        return;


    }

    public function fliegen(Request $reqeust) {
        if (Input::get('action') == 'add_pilot') {
            /* $this->validate($reqeust,
                     ['user_id' => 'unique:pilot,user_id',
                     ]);*/
            $pilot = new \App\models\pilot();
            $pilot->user_id = Input::get('user_id');
            $pilot->project_id = Input::get('project_id');
            $pilot->comment = Input::get('comment');
            $pilot->date = Input::get('date');

            $pilot->save();

            return;

        } else if (Input::get('action') == 'add_car') {
            $this->validate($reqeust,
                    ['seats' => 'required|numeric|min:1',
                    ]);

            $car = new \App\models\car;
            $car->user_id = Input::get('driver_id');
            $car->date = Input::get('date');
            $car->seats = Input::get('seats');
            $car->comment = Input::get('comment');
            $car->save();
            return;
        } else if (Input::get('action') == 'update_car') {
            $car = car::where('id', $reqeust->car_id)->get()->first();
            if ($car->seats - $car->mitfahrer->count() >= sizeof($reqeust->mitfahrer)) {
                $car->mitfahrer()->attach($reqeust->mitfahrer);
            }
            return;
        } else if (Input::get('action') == 'delete_car') {
            $car = car::find($reqeust->car_id);
            if (Gate::denies('update-car', $car)) {
                return;
            }
            $car->delete();
            return;
        }


        return;
    }
}
