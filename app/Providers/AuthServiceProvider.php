<?php

namespace App\Providers;

use App\models\palaverItem;
use Carbon\Carbon;
use App\models\car;
use App\models\entry;
use App\models\pilot;
use App\models\project;
use App\models\User;
use App\Policies\entryPolicy;
use App\Policies\projectPolicy;
use App\Policies\userPolicy;
use App\Policies\pilotPolicy;
use App\Policies\carPolicy;
use App\Policies\palaverItem_policy;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
            'App\Model'    => 'App\Policies\ModelPolicy',
            entry::class   => entryPolicy::class,
            project::class => projectPolicy::class,
            user::class    => userPolicy::class,
            car::class     => carPolicy::class,
            pilot::class   => pilotPolicy::class,
            palaverItem::class => palaverItem_policy::class,


    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $this->registerPolicies();
        //grant all rights to superuser
        $gate->before(function ($user, $abilty) {
            if ($user->id == 1) {
                return true;
            }
            return;
        });

        $gate->define('create-user', function ($user) {
            return $user->is_admin;
        });
        $gate->define('update-user', function (User $user, User $user2) {
            return $user->id === $user2->id;
        });
        $gate->define('palaver', function (User $user) {
            return $user->can_palaver;
        });

        $gate->define('view-profile', function (User $user, User $target) {
            return $user->id === $target->id;
        });



    }
}
