@extends('layouts.outer_form_partial')
@section('title','Aufgabe erstellen')


@section('content')

@section('panel_heading','Aufgabe erstellen')
@section('panel_body')

    @if(isset($_REQUEST['type']))

        {!!  Form::open(['method'=>'post','url' => 'palaverItem/?type='.$_REQUEST['type'],'class' => 'form-horizontal','role'=>'form']) !!}

    @else
        {!!  Form::open(['method'=>'post','url' => 'palaverItem/','class' => 'form-horizontal','role'=>'form']) !!}

    @endif

    @include('palaverItem.form_partial')


    @include('layouts.form_buttons_partial')

    <script>
        $('#btnReset').click(function () {
            $(".selectpicker").selectpicker('deselectAll');
            $('#helper_multiselect').selectpicker('refresh');
        })
    </script>

    {!! Form::close() !!}
@endsection

