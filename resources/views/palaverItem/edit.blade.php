@extends('layouts.outer_form_partial')

@section('title','Aufgabe bearbeiten')


@section('panel_heading','Aufgabe bearbeiten')

@section('panel_body')

    @if(isset($_REQUEST['type']))
        {!!  Form::model($palaverItem,['method'=>'patch','url' => 'palaverItem/'.$palaverItem->id.'?type='.$_REQUEST['type'],'class' =>'form-horizontal','role'=>'form']) !!}
    @else
        {!!  Form::model($palaverItem,['method'=>'patch','url' => 'palaverItem/'.$palaverItem->id,'class' =>'form-horizontal','role'=>'form']) !!}
    @endif

    @include('palaverItem.form_partial')

    @include('layouts.form_buttons_partial')


    {!! Form::close() !!}

@endsection

