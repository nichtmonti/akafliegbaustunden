@extends('layouts.layout')
@section('title','TODO')
@section('head')
    <style>
        .glyphicon.large {
            font-size: 35px;
        }

    </style>

@endsection

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="col-md-12">
        <?php $counter = 0?>
        <?php $max = 0;
        foreach ($projects as $project) {
            $max += $project->palaverItems->count(); //amount of palaverItems dispplayed
        }?>
        @foreach($projects as $project)

            <label for="{{$project->name}}_link" class="col-md-12 col-xs-12 project-title"
                   style="z-index: 100; position: relative">
                <div class="panel panel-default col-md-12 col-md-offset-0 ">
                    <a href="/project/{{$project->id}}/edit?type=np" style="color: black" id="{{$project->name}}_link">
                        <h2 class="col-md-10">{{$project->name}}</h2>
                    </a>
                </div>
            </label>


            @foreach($project->palaverItems as $palaverItem)
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default {{$palaverItem->id}}_idnav" id="div_{{$counter}}">
                        <div class="panel-heading">
                            <span style="color: black; cursor: pointer" onclick="edit({{$palaverItem->id}})">
                                <h4>{{$palaverItem->title}}</h4>
                            </span>
                        </div>
                        <div class="panel-body" id={{$palaverItem->id}}>


                            <div class="row col-md-6">
                                <dl class="dl-horizontal ">

                                    <h4>
                                        <dt>Verantwortlich</dt>
                                        <dd id="{{$palaverItem->id}}_users">{{$palaverItem->responsible_users}}</dd>

                                        <dt>Beschreibung</dt>
                                        <dd id="{{$palaverItem->id}}_description">{{$palaverItem->description}}</dd>

                                        <dt>Gesamtbauzeit</dt>
                                        <dd>{{$palaverItem->formatted_work_time}}</dd>
                                        <dt>Aktueller Status</dt>
                                        <dd id={{$palaverItem->id}}_status>{{$palaverItem->real_status}}</dd>

                                        <dt>Termin</dt>
                                        <dd id={{$palaverItem->id}}_termin>{{$palaverItem->date}}</dd>


                                    </h4>
                                </dl>
                            </div>
                            <div class="row col-md-6">
                                @if(!$palaverItem->entries->isEmpty())
                                    <table class="table table-hover  table-bordered">
                                        @foreach($palaverItem->entries->sortBy('date')->chunk(3)[0] as $entry)
                                            <tr>
                                                <td>{{$entry->user->short_name}}</td>
                                                <td> {{$entry->description}}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <?php $counter++?>
            @endforeach
            <div class="row  col-md-10 col-md-offset-1">


                <a href="/palaverItem/create?project={{$project->id}}&type=np" class="btn btn-warning col-md-offset-5">
                    <h5>
                        Weiteren Eintrag
                        erstellen</h5></a>

            </div>
        @endforeach

    </div>
@endsection

@section('afterBody')
    <script>
        var pointer_pos = 0;
        var cur_id = -1; //track if something is currently being updated
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            }
        });


        $(document).ready(function () {
            $(".project-title").stick_in_parent();
            @if(Session::has('hook'))
            //  document.getElementsByClassName({{Session::get('hook')}}+"_idnav")[0].scrollIntoView();
            window.scrollTo(0, $('.{{Session::get('hook')}}_idnav').offset().top - 100);
            //TODO set pointer position to correct position
            @endif

        $('body').keydown(function (event) {
                if (event.keyCode == 13) {//Enter
                    event.preventDefault();
                    if (cur_id == -1) {
                        return;
                    }
                    else {
                        save();
                        return;
                    }
                }
            });
        });

        function save() {

            var id = cur_id;
            //fetch new text for description
            var new_desc = $('#description_edit').val();
            var new_status = $('#status_edit').val()
            var new_termin = $('#termin_edit').val();

            //get selected ids

            var ids = [];
            $('.selectpicker option:selected').each(function () {
                ids.push($(this).val());
            });

            //send ajax to save new values
            $.post("/palaverItem/" + id + "/ajax", {
                        'action': 'edit_all',
                        'description': new_desc,
                        'helpers': ids,
                        'status': new_status,
                        'termin': new_termin
                    },
                    function (data) {
                        $('#' + id + '_users').html(data[0]);
                        $('#' + id + '_description').html(data[1]);
                        $('#' + id + '_status').html(data[2]);
                        $('#' + id + '_termin').html(data[3]);

                    })
            cur_id = -1;
        }

        function edit(id) {
            if (cur_id == id) {
                save();
                return;
            }
            else if (cur_id != -1) {
                alert("Bitte Änderungen zuerst speichern (Enter drücken zum speichern)");
                return false;
            }
            cur_id = id;

            //make multiselect editable
            $.post("/palaverItem/" + id + "/ajax", {
                        'action': 'user_form',
                    },
                    function (data) {
                        $('#' + id + '_users').html(data);
                    })

            //make description editable
            cur_val = $('#' + id + '_description').html();
            var newHTML = '<textarea class="form-control" rows="2" name="description" cols="50" id="description_edit">' + cur_val + '</textarea>';
            $('#' + id + '_description').html(newHTML);


            //make status editable
            cur_status = $('#' + id + '_status').html();
            if (cur_status != "Fertig" && cur_status != "Abgebrochen") {
                var newHTML = '<textarea class="form-control" rows="2" name="description" cols="50" id="status_edit">' + cur_status + '</textarea>';
                $('#' + id + '_status').html(newHTML);
            }

            //make termin editable
            var cur_termin = $('#' + id + '_termin').html();
            var newHTML = '<textarea class="form-control" rows="2" name="termin" cols="50" id="termin_edit">' + cur_termin + '</textarea>';
            $('#' + id + '_termin').html(newHTML);
            //TODO create option for done

            return true;
        }
    </script>
@endsection