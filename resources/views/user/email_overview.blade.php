@extends('layouts.layout')

@section('title')
    Email Adressen
@endsection

@section('content')
    <div class="col-md-12">
        <h2>Email Adressen</h2>
        <table class="table table-hover">
            <thead>
            <tr>
                <td>Name</td>
                <td>Email</td>
            </tr>
            </thead>
            <tbody>

            @foreach($users as $user)
                <tr>
                    <td>{{$user->full_name}}</td>
                    <td>{{$user->email}}</td>
                </tr>
            @endforeach
            </tbody>

        </table>
    </div>

@endsection