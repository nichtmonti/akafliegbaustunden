@extends('layouts.layout')
@section('title')
    Baustunden einsehen
@endsection

@section('content')
    @include('partials.show_resigned_')
    <table class="table table-hover  table-bordered">
        <thead>
        <tr>
            <td>Name</td>
            <td>Relativbauzeit</td>
            <td>Gesamtbauzeit</td>
            <td>Flugbrechtigt</td>
            @unless(Auth::guest())
                @if(Auth::user()->is_admin)
                    <td>edit</td>
                @endif
            @endunless
        </tr>
        </thead>
        @foreach($users as $user)
            <tr>
                <td>

                    <a href='/user/{{$user->id}}@if(isset($year))?year={{$year}}@endif'>{{$user->full_name}}</a>
                </td>
                <td>{{$user->formattedRelativeWorkTime($year)}}</td>
                <td>{{$user->getFormattedWorkTimeAttribute($year)}}</td>
                <td>
                    @if($user->getFlugberechtigtAttribute(\Help::currentYear()))
                        Ja
                    @else
                        Nein
                    @endif
                </td>
                @unless(Auth::guest())
                    @if(Auth::user()->is_admin)
                        <td>
                            <a href="/user/{{$user->id}}/edit">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                        </td>
                    @endif
                @endunless
            </tr>
        @endforeach
    </table>
	{{-- empty path required for correct URL resolution --}}
    {{ $users->appends(\Input::except('page'))->setPath('')->links() }}
<!--erhält parameter year zwischen den Seiten -->

@endsection
