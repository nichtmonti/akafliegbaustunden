@extends('layouts.layout')



@section('title')
    Baustunden {{$user->first_name}}
@endsection


@section('content')
    <div class="row">
        <div class="col-md-2">
            @if(env('ALLOW_PAST_YEARS'))
                <div>
                    @include('partials.year_select_')
                </div>
            @endif
            @if(Auth::user()->admin||Auth::id()===$user->id)
                <div>
                    @if(isset($edit)&&$edit=="true")
                        <a href='/user/{{$user->id}}?year={{$year}}&edit=false'>
                            <button class="btn btn-default">
                                Bearbeiten beenden <span class="glyphicon glyphicon-pencil" style="vertical-align:middle"></span>
                            </button>
                        </a>
                    @else
                        <a href='/user/{{$user->id}}?year={{$year}}&edit=true'>
                            <button class="btn btn-default">
                                Einträge bearbeiten<span class="glyphicon glyphicon-pencil"
                                                         style="vertical-align:middle"></span>
                            </button>
                        </a>
                    @endif


                </div>
            @endif
        </div>
        <div class="col-md-8">
            <h2 style=text-align:center;>   {{$user->fullName}}</h2>
            <div class="panel panel-default">
                <div class="panel-body">
                    <dl class="dl-horizontal">
                        <h4>
                            <dt>Status</dt>
                            <dd>{{$user->status->name}}</dd>

                            <dt>Erforderliche Stunden/Monat</dt>
                            <dd>{{\Help::format_time($user->status->required_hrs*60)}}</dd>

                            <dt>Gesamtbauzeit</dt>
                            <dd>{{$user->getFormattedWorkTimeAttribute($year)}}</dd>

                            <dt>Soll-Arbeitszeit</dt>
                            <dd>{{\Help::format_time($user->getRequiredWorkTime($year)*60)}}</dd>

                            <dt>Relativbauzeit</dt>
                            <dd>{{$user->formattedRelativeWorkTime($year)}}</dd>

                            @if($year == Help::currentYear())
                                <dt>Aktuelle Aufgaben</dt>
                                <dd>{!! $user->palaver_item_string!!}</dd>

                                <dt>Flugberechtigt</dt>
                                <dd>
                                    @if($user->getFlugberechtigtAttribute($year))
                                        Ja
                                    @else
                                        Nein
                                    @endif
                                    @endif
                                </dd>
                        </h4>
                    </dl>
                </div>
            </div>
        </div>
    </div>


    @if($user->entries->isEmpty())
        <div class="row">
            <h3>Noch keine Einträge für {{$user->full_name}}</h3>
        </div>
    @else

        <table class="table table-hover table-bordered">


            <thead>
            <tr>
                <td>Datum</td>
                <td>Mitarbeiter</td>
                <td>Beschreibung</td>
                <td>Projekt</td>
                <td>Aufgabe</td>
                <td>Arbeitszeit</td>

                @unless(Auth::guest())
                    @if((Auth::user()->admin||Auth::id()===$user->id) &&isset($edit)&&$edit=="true")
                        <td>edit</td>
                    @endif
                @endunless
            </tr>
            </thead>
            @foreach($entries as $entry)
                <tr>
                    <td>{{\Carbon\Carbon::parse($entry->date)->format('d.m.Y')}}</td>

                    <td>
                        {{$entry->helpers_string}}
                    </td>
                    <td>{{$entry->description}}</td>
                    <td>{{$entry->palaverItem->project->name}}</td>
                    <td>{{$entry->palaverItem->title}}</td>
                    <td>{{$entry->formatted_time}}</td>
                    @if(isset($edit)&&$edit=="true")
                        <td>

                            @can('update',$entry)
                            <a class="btn btn-warning" style="float: left; display: inline; margin-right: 5%"
                               href="/entry/{{$entry->id}}/edit">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <form action="/entry/{{$entry->id}}" method="post" style="display: inherit">
                                {{ csrf_field() }}
                                {{method_field('DELETE')}}
                                <button class="btn btn-danger" href="#" type="submit"
                                        onclick="return confirm('Eintrag wirklich löschen?')">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </button>
                            </form>

                            @endcan

                        </td>
                    @endif
                </tr>
            @endforeach
        </table>
        @endif
        {{$entries->appends(\Input::except('page'))->setPath('')->links()}} <!--erhält parameter year zwischen den Seiten -->
@endsection
@section('afterBody')

@endsection
