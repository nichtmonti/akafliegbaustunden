<select id="year-select" class="form-control">
    @if(!empty($user->created_at))
        {{$x=Help::currentYear()}}
        @for($x; $x >= \Carbon\Carbon::parse($user->created_at)->year; $x--)
            <option value="{{$x}}"
                    @if($x == $year)
                    selected
                    @endif
            >{{$x-1}}/{{$x}}</option>
        @endfor
    @else
        <option value="{{$year}}">{{$year-1}}{{$year}}</option>
    @endif
</select>

@push('footer')
<script>
    $(document).ready(function () {
        $('#year-select').change(function () {
            window.location = '/user/{{$user->id}}' + '?year=' + $('#year-select').val() + '&edit=' + $('#edit-select').is(':checked');
        });
    });
</script>
@endpush
