@extends('layouts.layout')
@section('content')
    <h2>Einträge {{$year}}</h2>
    <table class="table table-hover  table-bordered">
        <thead>
        <tr>
            <td>Datum</td>
            <td>Name</td>
            <td>Beschreibung</td>
            <td>Helfer</td>
            <td>Stunden</td>
            @if(isset($edit) && $edit)
                <td>Bearbeiten</td>
            @endif

        </tr>
        </thead>
        @foreach($entries as $entry)
            <tr>
                <td>{{Help::formatDate($entry->date)}}</td>
                <td>{{$entry->user->short_name}} </td>
                <td>{{$entry->description}}</td>
                <td>{{$entry->helpers_string}}</td>
                <td>{{$entry->formatted_time}}</td>
                @if(isset($edit)&&$edit)
                    @can('edit',$entry)
                        <td>
                            <a href="entry/{{$entry->id}}/edit">bearbeiten</a>
                        </td>
                    @endcan
                @endif

                @endforeach
            </tr>
    </table>
@endsection