@extends('layouts.outer_form_partial')
@section('title','Eintrag bearbeiten')

@section('head')

@endsection

@section('panel_heading','Eintrag bearbeiten')

@section('panel_body')

    {!!  Form::model($entry,['method'=>'patch','url' => 'entry/'.$entry->id,'class' =>'form-horizontal']) !!}

    @include('entry.form_partial')

    @include('layouts.form_buttons_partial')

        <script>
            $('#btnReset').click(function () {
                $(".selectpicker").selectpicker('deselectAll');
                $('#helper_multiselect').selectpicker('refresh');
            })
        </script>


    {!! Form::close() !!}

@endsection

