@if(!isset($entry)&&!Auth::user()->is_admin)
    {{Form::hidden('user_id',Auth::id())}} {{--TODO bessere Lösung für das disabled field finden--}}
@endif
<div class="form-group">
    {!!Form::label('user_id','Name',['class' => 'control-label col-sm-2 pull-left'])!!}
    <div class="col-sm-8 col-sm-offset-1">
        @if(!isset($entry)&&Auth::user()->is_admin)
            {!!Form::select('user_id',$users,Auth::user()->id,['class' => 'selectpicker form-control','data-live-search'=>'true','required' => 'required','size'=>'8'])!!}
        @elseif(!isset($entry))
            {!!Form::select('user_id',[Auth::user()->id => Auth::user()->full_name],Auth::user()->id,['class' => 'selectpicker form-control','data-live-search'=>'true','required' => 'required','size'=>'8','disabled'])!!}
        @else
            {!!Form::select('user_id',$users,NULL,['class' => 'selectpicker form-control','data-live-search'=>'true','required' => 'required','size'=>'8'])!!}
        @endif
    </div>
</div>

<div class="form-group ">
    {!!Form::label('date','Datum',['class' => 'control-label col-sm-2'])!!}
    <div class="col-sm-8 col-sm-offset-1">
        <div class='input-group date' id='datetimepicker1'>
            {!!Form::text('date','',['class' => 'form-control col-sm-10 ' ,'required' => 'required'])!!}
            <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
        </div>
        @include('layouts.display_error',['field_name'=>'date'])

    </div>
    <script type="text/javascript">

        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: "DD.MM.YYYY",
                @if(isset($entry))
                date: moment("{{$entry->date}}"),
                @else
                date: moment()
                @endif
            });
        });
    </script>
</div>


<div class="form-group">
    {!!Form::label('palaverItem_id','Aufgabe',['class' => ' control-label col-sm-2 pull-left'])!!}
    <div class="col-sm-8 col-sm-offset-1">
        <select class="form-control selectpicker" name="palaverItem_id" id="palaverItem_id" data-live-search="true">
            <option></option>
            {{$selected=false}}
            @foreach($projects as $project)
                <optgroup label="{{$project->name}}">
                    @foreach($project->palaverItems as $palaverItem)
                        @if(isset($entry) && $entry->palaverItem->id == $palaverItem->id)
                            <?php $selected=true ?>
                        @endif
                        <option value={{$palaverItem->id}} {{$selected ?'selected=selected' : ''}} >{{$palaverItem->title}}</option>
                        <?php $selected = false ?>
                    @endforeach
                </optgroup>
            @endforeach
        </select>
        @include('layouts.display_error',['field_name'=>'palaverItem_id'])
        @can('create', 'PalaverItem:class')
            <a href="/palaverItem/create">Neue Aufgabe erstellen</a>
        @endcan

    </div>
</div>


<div class="form-group">
    {!!Form::label('description','Beschreibung',['class' => 'control-label col-sm-2 pull-left'])!!}
    <div class="col-sm-8 col-sm-offset-1">
        {!!Form::textarea('description',NULL,['class' => 'form-control','rows'=>'3','required' => 'required','maxlength'=>'1000'])!!}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group">
    {!!Form::label('helper_multiselect','Mithelfer',['class' => 'control-label col-sm-2',])!!}
    <div class="col-sm-8 col-sm-offset-1">
        @if(isset($entry))
            {!!Form::select('helper_id[]',$users,$entry->helpers()->pluck('users.id')->all(),['class' => 'selectpicker form-control', 'id' =>'helper_multiselect','data-live-search'=>'true','multiple'=>'multiple','data-selected-text-format' =>'count','data-size'=> '12'])!!}
        @else
            {!!Form::select('helper_id[]',$users,NULL,['class' => 'selectpicker form-control','data-live-search'=>'true', 'id' =>'helper_multiselect','multiple'=>'multiple','data-selected-text-format' =>'count','data-size'=> '12'])!!}
        @endif

    </div>
</div>


<script>
    $('.selectpicker').selectpicker({

        noneSelectedText: 'Keine',
        countSelectedText: function (x, y) {
            return x + ' Mitglieder ausgewählt';
        }
    });
</script>
<div class="form-group">
    {!!Form::label('hours','Arbeitszeit',['class' => 'control-label col-sm-2'])!!}
    @if(isset($entry))
        <div class="col-sm-4 col-sm-offset-1 ">
            {!!Form::text('hours',$entry->hours,['class' => 'form-control','required' => 'required','placeholder' =>'Stunden'])!!}
            @include('layouts.display_error',['field_name'=>'hours'])

        </div>
        <div class="col-sm-4">
            {!!Form::text('minutes',$entry->minutes,['class' => 'form-control','required' => 'required','placeholder' =>'Minuten'])!!}
            @include('layouts.display_error',['field_name'=>'minutes'])
        </div>
    @else
        <div class="col-sm-4 col-sm-offset-1 ">
            {!!Form::text('hours',NULL,['class' => 'form-control', 'placeholder' =>'Stunden'])!!}
            @include('layouts.display_error',['field_name'=>'hours'])
        </div>
        <div class="col-sm-4">
            {!!Form::text('minutes',NULL,['class' => 'form-control','placeholder' =>'Minuten'])!!}
            @include('layouts.display_error',['field_name'=>'minutes'])
        </div>
    @endif
</div>



