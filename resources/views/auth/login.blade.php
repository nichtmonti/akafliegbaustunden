@extends('layouts.layout')
<div id="app"></div>
@section('content')
    <div class="container" style="margin: auto">
        <div class="row" >

            <div class="col-md-8 col-md-offset-2" style="margin-top: 10%">
                <div class="panel panel-default">
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">

                        <form class="form-horizontal" role="form" method="POST" action="/login">
                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label class="col-md-4 control-label">Nachname</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="last_name" value="{{ old('name') }}">
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Passwort</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('remember','Angemeldet bleiben',['class' => 'control-label col-md-4'])!!}
                                <div class="col-md-6 ">
                                    {!!Form::checkbox('remember')!!}
                                </div>
                            </div>

                            <strong>{{$errors->first()}}</strong>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-sign-in"></i>Login
                                    </button>
                                <!--
                                    <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your
                                        Password?</a>-->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <h4 style="text-align: center">Durch das Einloggen akzeptiere ich die <a href="/datenschutz">Datenschutzerklärung</a></h4>

    </div>
@endsection
