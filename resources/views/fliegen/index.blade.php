@extends('layouts.layout')
@section('title','ICH WILL FLIEGEN')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <h3>Fliegen {{$date}}</h3>

    <!-- TODO kompletter rwork inkl. databse model. Flugzeuge zu Textfeld ändern, user darf nur sich selbst eintragen für fliegen/Auto. Zugriff auf ->flugberechtigt minimieren (admin darf alle zum fliegen anmelden o.ä.-->

    <div class="row col-md-4">
    @include('fliegen.date_select_')

    <!-- Flugzeug Info und Autos -->

        @include('fliegen.airplanes_')

        @include('fliegen.cars_')
    </div>

    <div class="row col-md-7 col-md-offset-1">

        @include('fliegen.pilots_')

    </div>

@endsection

@section('afterBody')
    <script>
        @stack('js')
                $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();

            @stack('js_ready')
        });
    </script>
@endsection