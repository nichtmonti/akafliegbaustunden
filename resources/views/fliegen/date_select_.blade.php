<div class="form-group">
    <div class='input-group date' id='date-select'>
        <input type='text' class="form-control" id='date'/>
        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
        </div>
    </div>


@push('js_ready')
$("#date-select").on("dp.change", function (e) {
document.location.href = '/fliegen' + '?date=' + $('#date-select').data().date;
});
@endpush

@push('js')
$(function () {
$('#date-select').datetimepicker({
format: "YYYY-MM-DD",
@if(isset($date))
    date: moment("{{$date}}", "YYYY-MM-DD"),
@else
    date: moment()
@endif
});
});
@endpush