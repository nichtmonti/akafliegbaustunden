@foreach($users as $user)
    <tr>

        <td><a href='/user/{{$user->id}}'>{{$user->full_name}}</a></td>

        <td>
            <!-- Anwesende -->
            <label style="display: block">
                @if(isset($palaver))
                    {!!Form::radio($user->id,2,$user->anwesend($palaver->id),1)!!}
                @else
                    <!-- TODO default anwesend oder abwesend -->
                    {!!Form::radio($user->id,2,1)!!}
                @endif
            </label>
        </td>
        <td>
            <!-- Entschuldigte -->
            <label style="display: block">
                @if(isset($palaver))
                    {!!Form::radio($user->id,1,$user->entschuldigt($palaver->id))!!}
                @else
                    {!!Form::radio($user->id,1,$user->next_entschuldigt)!!}
                @endif
            </label>
        </td>
        <td>
            <!-- nicht entschuldigte -->
            <label style="display: block">
                @if(isset($palaver))
                    {!!Form::radio($user->id,'a')!!}
                @else
                    {!!Form::radio($user->id,'a')!!}
                @endif
            </label>

        </td>
    </tr>
@endforeach




