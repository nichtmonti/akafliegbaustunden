<!DOCTYPE html>
<html lang="de">
<head>
  <title>@yield('title')</title>

  @stack('head')

    <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="{{asset('js/app.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset("css/app.css")}}">

  @if(env('PALAVER_FEATURES'))
    <script src="https://cdn.rawgit.com/leafo/sticky-kit/v1.1.2/jquery.sticky-kit.min.js"></script>
  @endif

@yield('head')


</head>
<body>
@if(Auth::check())
  <div class="container-fluid">
    <!-- TODO Menü spannt nicht gesamten Bildschirm -->
    @include('layouts.navbar_partial')
  </div>
@endif
<div class="container">
  @include('partials.flash_message')

    @if(Auth::check() && Auth::user()->flugverbot)
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <h3>Du hast Flugverbot!</h3>
            Grund: {{ Auth::user()->flugverbot_kommentar }}
        </div>
    @endif
  @yield('content')
</div>

</body>
@stack('footer')
@yield('afterBody')
</html>
