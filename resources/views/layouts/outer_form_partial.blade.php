@extends('layouts.layout')
@section('head')
    @yield('head')
@endsection
@section('content')
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    @yield('panel_heading')
                </h3>
            </div>
            <div class="panel-body ">
                <div class="col-md-12">
                    @yield('panel_body')
                </div>
            </div>
        </div>
    </div>
@endsection



