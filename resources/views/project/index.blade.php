@extends('layouts.layout')


@section('title','Übersicht')

@section('content')

    <div style="margin-bottom: 3%">
        @include('project.show_all_toggle_')
    </div>
    <div class="row col-md-4 col-md-offset-4" >

        <?php setlocale(LC_TIME, 'German') ?>

        <h3>Meiste Baustunden im {{\Carbon\Carbon::today()->subMonthNoOverflow()->formatLocalized('%B')}}</h3>
            <table class='table'>
        @foreach($top3 as $top)

                    <tr>
        @if($top->nickname=="")
            <h4><td>{{$top->first_name}}</td> <td>{{Help::format_time($top->Zeit)}}</td></h4>
        @else
            <h4><td>{{$top->nickname}}</td> <td>{{Help::format_time($top->Zeit)}}</td></h4>
        @endif
                    </tr>
        @endforeach
        </table>
    </div>
    <div class="row">
        @foreach($projects as $project)
            <div class="col-md-6">
                @include('project.overview_')
            </div>
        @endforeach

    </div>

@endsection
