<div class="form-group">
    {!!Form::label('name','Name',['class' => 'control-label col-sm-2 pull-left'])!!}
    <div class="col-sm-8 col-sm-offset-1">
        {!!Form::text('name',NULL,['class' => 'form-control','required' => 'required'])!!}
        @include('layouts.display_error',['field_name'=>'name'])
    </div>
</div>

<div class="form-group">
    {!!Form::label('user_id','Betreuer',['class' => 'control-label col-sm-2 pull-left'])!!}
    <div class="col-sm-8 col-sm-offset-1">
        @if(Auth::check())
            {!!Form::select('user_id',$users,NULL,['class' => 'selectpicker form-control','data-live-search'=>'true','required' => 'required','size'=>'8'])!!}
        @else
            {!!Form::select('user_id',$users,NULL,['class' => 'selectpicker form-control','data-live-search'=>'true','required' => 'required','size'=>'8'])!!}
        @endif
    </div>
</div>


<div class="form-group">
    {!!Form::label('description','Status',['class' => 'control-label col-sm-2 pull-left'])!!}
    <div class="col-sm-8 col-sm-offset-1">
        {!!Form::textarea('description',NULL,['class' => 'form-control','rows'=>'3'])!!}
    </div>
</div>


@if(!isset($project) || $project->isAirplane)
    <div class="form-group">
        {!!Form::label('flightReady','Flugklar',['class' => 'control-label col-sm-2'])!!}
        <div class="col-sm-8 col-sm-offset-1 ">
            {!!Form::checkbox('flightReady')!!}
        </div>
    </div>


<div class="form-group">
    {!!Form::label('isAirplane','Flugzeug',['class' => 'control-label col-sm-2'])!!}
    <div class="col-sm-8 col-sm-offset-1 ">
        {!!Form::checkbox('isAirplane')!!}
    </div>
</div>
@endif

@if(Auth::user()->is_admin || !isset($project))
    <div class="form-group">
        {!!Form::label('active','Aktiv',['class' => 'control-label col-sm-2'])!!}
        <div class="col-sm-8 col-sm-offset-1 ">
            {!!Form::checkbox('active')!!}
        </div>
    </div>
@endif
